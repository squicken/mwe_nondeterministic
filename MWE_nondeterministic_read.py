#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Minimal workin example for non-deterministic behavior of loading results
"""

# Import statements
from fenics import *

__author__ = 'sjeng'
__email__ = 's.quicken@maastrichtuniversity.nl'
__date__ = '2018-04-19'

# Last Modified by:   sjeng
# Last Modified time: 2018-04-19

# Setting FEniCS parameters to values used in original script

# Allow extrapolation This is REQUIRED for export of WSS
parameters['allow_extrapolation'] = True

# Mesh partitioning library
parameters['reorder_dofs_serial'] = True
parameters['mesh_partitioner'] = "SCOTCH"
parameters['dof_ordering_library'] = "SCOTCH"

mesh_file = 'mesh.xdmf'
hdf5_file = 'file.hdf5'
outfile = 'out.xdmf'


# Read the mesh file
mesh = Mesh()
xdmf = XDMFFile(mesh.mpi_comm(), mesh_file)  # Specify that the mesh is XDMF
xdmf.read(mesh)  # Read the mesh from XDMF file

# Create a function space
tmp_space = FunctionSpace(mesh, 'CG', 2)
# Create a vector in which the HDF5 file will be read
vec_tmp = Function(tmp_space)

# Read the HDF5 File
hdf5_file = HDF5File(mpi_comm_world(), hdf5_file, "r")
hdf5_file.read(vec_tmp.vector(), "/current", False)

# Write the result to XDMF, differences w.r.t. the original vector can be
# observed in Paraview
outXDMF = XDMFFile(mpi_comm_world(), outfile)
outXDMF.write(vec_tmp, 0.)
