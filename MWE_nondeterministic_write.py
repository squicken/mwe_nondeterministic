#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Minimal workin example for non-deterministic behavior of loading results
"""

# Import statements
from fenics import *

__author__ = 'sjeng'
__email__ = 's.quicken@maastrichtuniversity.nl'
__date__ = '2018-04-19'

# Last Modified by:   sjeng
# Last Modified time: 2018-04-19

# Setting FEniCS parameters to values used in original script

# Allow extrapolation This is REQUIRED for export of WSS
parameters['allow_extrapolation'] = True

# Mesh partitioning library
parameters['reorder_dofs_serial'] = True
parameters['mesh_partitioner'] = "SCOTCH"
parameters['dof_ordering_library'] = "SCOTCH"

mesh_file = 'mesh.xdmf'
hdf5_file = 'file.hdf5'

outfile = 'out0.xdmf'

# Read the mesh file
mesh = Mesh()
hdf5_file = HDF5File(mesh.mpi_comm(), hdf5_file, 'w')
xdmf = XDMFFile(mesh.mpi_comm(), mesh_file)  # Specify that the mesh is XDMF
xdmf.read(mesh)  # Read the mesh from XDMF file

# Create a function space
tmp_space = FunctionSpace(mesh, 'CG', 2)

# Create a vector that holds the MPI rank per node
tmp = interpolate(Constant(MPI.rank(mpi_comm_world())) * 1., tmp_space)

# Write the vector to HDF5
hdf5_file.write(tmp.vector(), '/current')

# Write the rank to a XDMF file. When reading back the vector from HDF5 and
# saving again to XDMF, the results should be different (inspect in paraview)
outXDMF = XDMFFile(mpi_comm_world(), outfile)
outXDMF.write(tmp, 0.)
